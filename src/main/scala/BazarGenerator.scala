import Domino._
import scala.util.{Random => Rand}
import Domino.edgeProducer
import Domino.twiceValencyEdgeProducer

/**
 * Генератор базаров
 */
object BazarGenerator {

  sealed trait GenerationStrategy
  case object Random extends GenerationStrategy
  case object Domino extends GenerationStrategy
  case object Line extends GenerationStrategy
  case object Ring extends GenerationStrategy

  lazy val bazar = (for {
    i <- 0 to 6
    j <- 0 to 6 if i <= j
  } yield Bone(i, j)).toList

  /**
   * Сгенерировать базар с заданным количеством костяшек.
   *
   * @param size Количество костяшек.
   *             Значение должно лежать в диапазоне [0, 28].
   *             Значение по умолчанию 27.
   * @param strategy Стратегия генерации базара.
   *                 * [[Random]] - базар, состоящий из случайного набора костяшек.
   *                 Результирующий набор не содержит дубликатов.
   *                 Является значением по умолчанию.
   *                 * [[Domino]] - базар, костяшки которого можно соединить по
   *                 правилам домино, без остатка.
   *                 Результирующий набор может содержать дубликаты.
   *                 * [[Line]] - базар, костяшки которого можно выстроить в ряд.
   *                 Результирующий набор может содержать дубликаты.
   *                 * [[Ring]] - базар, костяшки которого можно замкнуть в кольцо.
   *                 Результирующий набор может содержать дубликаты.
   * @return Базар, сгенерированный по переданным параметрам.
   */
  def generate(size: Int = 27, strategy: GenerationStrategy = Random): Bazar = {
    require(size >= 0 && size <= bazar.size, s"Illegal bazar size: $size")

    def mkBone(rand: Rand, graphChecker: ConnectedGraphChecker, edgeProducer: Bone => Edge): Bone = {
      def tryMkBone: Option[Bone] = {
        val bone = bazar(rand.nextInt(bazar.size))
        val edge = edgeProducer(bone)
        graphChecker.addEdge(edge) match {
          case NoWays => None
          case _ => Some(bone)
        }
      }
      Stream.continually(tryMkBone).collectFirst {
        case Some(bone) => bone
      }.head
    }

    def gen(size: Int, edgeProducer: Bone => Edge): Bazar = {
      val graphChecker = new ConnectedGraphChecker
      val rand = new Rand
      Stream.continually(mkBone(rand, graphChecker, edgeProducer)).take(size).toList
    }

    def generateRandom(size: Int): Bazar =
      Rand.shuffle(bazar).take(size)

    def generateDomino(size: Int): Bazar =
      gen(size, twiceValencyEdgeProducer)

    def generateLine(size: Int): Bazar =
      gen(size, edgeProducer)

    def generateRing(size: Int): Bazar = {
      val graphChecker = new ConnectedGraphChecker
      val rand = new Rand
      (for { i <- 1 to size } yield {
        if (i == size) {
          val slots = graphChecker.emptySlots.toList
          assert(slots.size == 2 || slots.size == 1)
          Bone(slots.head, if (slots.size == 2) slots(1) else slots.head)
        } else {
          mkBone(rand, graphChecker, edgeProducer)
        }
      }).toList
    }

    strategy match {
      case Random =>
        generateRandom(size)
      case Domino =>
        generateDomino(size)
      case Line =>
        generateLine(size)
      case Ring =>
        generateRing(size)
    }
  }
}

