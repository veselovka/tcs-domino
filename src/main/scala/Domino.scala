/**
 * Задачу о расположении костяшек базара по правилам домино можно свести к задаче проверки графа на связность,
 * где ребрами графа являются костяшки домино. Для этого был реализован класс [[Domino.ConnectedGraphChecker]].
 * Для поступающего на вход базара генерируются возможные перестановки костяшек, которые затем последовательно
 * проверяются, до тех пор пока не будет найден подходящий вариант.
 * Т.к. задачи получить сам граф не стоит, то будем хранить граф в виде хеш-таблицы, где ключ - значение точки графа,
 * а значение - текущая валентность точки, т.е. сколько еще ребер можно соединить с данной точкой. Валентность точек
 * нам нужна, т.к. для точек дубля значение валентности будет больше на 1 по сравнению с валентностью точек обычной
 * костяшки.
 * Если все ребра графа удалось добавить и при этом граф остался связным, то значит переданный на вход базар можно
 * соединить по правилам домино.
 *
 * Задача о расположении костяшек в линию аналогична задаче с домино с той лишь разницей, что валентность точек дубля
 * такая же как и валентность точек обычной костяшки.
 *
 * Задача о расположении костяшек в кольцо аналогична задачи с линией с той лишь разницей, что при добавлении в связный
 * граф последней костяшки она может быть присоединена к графу любой из сторон. В терминах теории графов это задача
 * о том содержит ли граф эйлеров цикл.
 */
object Domino {

  /**
   * Ребро графа
   */
  case class Edge(point1: Point, point2: Point) {
    def revert = Edge(point2, point1)
    val isDouble = point1.value == point2.value 
  }

  /**
   * Точка графа
   * @param value Значение
   * @param maxValency Максимальная валентность точки
   */
  case class Point(value: Int, maxValency: Int) {
    require(maxValency > 0, "Valency should be positive")
  }

  /**
   * Костяшка набора домино
   */
  case class Bone(value1: Int, value2: Int) {
    val isDouble = value1 == value2
  }


  type Bazar = List[Bone]


  sealed trait WaysToAdd
  case object NoWays extends WaysToAdd
  case object OneWay extends WaysToAdd
  case object TwoWays extends WaysToAdd


  /**
   * Класс, с помощью которого осуществляется проверка графа на связность.
   */
  class ConnectedGraphChecker() {
    private var valencyByPoint = Map.empty[Int, Int].withDefaultValue(0)
    private var prev: Map[Int, Int] = _

    /**
     * Добавить ребро к графу, если возможно, сохранив его связность.
     * @param edge Ребро
     * @return Способ, которым можно добавить ребро.
     *         * [[NoWays]] - Если не удалось добавить ребро
     *         * [[OneWay]] - Если ребро можно добавить только одной стороной
     *         * [[TwoWays]] - Если ребро можно добавить любой из сторон.
     *         В этом случае ребро будет присоединено с помощью [[Edge.point1]]
     */
    def addEdge(edge: Edge): WaysToAdd = {
      if (valencyByPoint.isEmpty) {
        prev = valencyByPoint
        addPoint(edge.point1)
        addPoint(edge.point2)
        OneWay
      } else {
        (valencyByPoint(edge.point1.value) > 0, valencyByPoint(edge.point2.value) > 0) match {
          case (true, true) =>
            prev = valencyByPoint
            connectPoint(edge.point1)
            addPoint(edge.point2)
            TwoWays
          case (true, false) =>
            prev = valencyByPoint
            connectPoint(edge.point1)
            addPoint(edge.point2)
            OneWay
          case (false, true) =>
            prev = valencyByPoint
            connectPoint(edge.point2)
            addPoint(edge.point1)
            OneWay
          case (false, false) =>
            NoWays
        } 
      }
    }

    def previous = {
      val checker = new ConnectedGraphChecker
      checker.valencyByPoint = this.prev
      checker
    }
    
    def emptySlots: Iterable[Int] = valencyByPoint.filter(_._2 > 0).keys

    private def connectPoint(point: Point): Unit =
      valencyByPoint = valencyByPoint + ((point.value, valencyByPoint(point.value) - 1 + point.maxValency - 1))

    private def addPoint(point: Point): Unit =
      valencyByPoint = valencyByPoint + ((point.value, valencyByPoint(point.value) + point.maxValency))
  }

  /**
   * Можно ли соединить все костяшки базара по правилам домино
   */
  def isDomino(bazar: Bazar): Boolean = {
    checkBazar(bazar, twiceValencyEdgeProducer, isConnectedGraph)
  }

  /**
   * Можно ли выстроить костяшки в ряд
   */
  def isLine(bazar: Bazar): Boolean = {
    checkBazar(bazar, edgeProducer, isConnectedGraph)
  }

  /**
   * Можно ли замкнуть костяшки в кольцо
   */
  def isRing(bazar: Bazar): Boolean = {
    checkBazar(bazar, edgeProducer, hasEulerianCycle)
  }

  def edgeProducer(bone: Bone): Edge =
    Edge(Point(bone.value1, 1), Point(bone.value2, 1))

  def twiceValencyEdgeProducer(bone: Bone): Edge = {
    if (bone.isDouble) {
      val point = Point(bone.value1, 2)
      Edge(point, point)
    }
    else Edge(Point(bone.value1, 1), Point(bone.value2, 1))
  }

  private def checkBazar(bazar: Bazar, edgeProducer: Bone => Edge, checkFunc: List[Edge] => Boolean): Boolean = {
    def checkPreconditions(bazar: Bazar): Unit =
      require(bazar.size > 2 && bazar.size < 28, s"Illegal bazar size=${bazar.size}")

    def graphEdges(bazar: Bazar, edgeProducer: Bone => Edge): List[Edge] =
      bazar.map(edgeProducer)

    def combinations(bazar: Bazar): Iterator[Bazar] =
      bazar.permutations

    checkPreconditions(bazar)

    combinations(bazar) exists {
      case bzr =>
        val res = checkFunc(graphEdges(bzr, edgeProducer))
//        if (res) println(s"Bazar: $bzr")
        res
    }
  }

  private def isConnectedGraph(edges: List[Edge]): Boolean = {
    def isConnected(edge: Edge, edgeTail: List[Edge], graphChecker: ConnectedGraphChecker, allowRevert: Boolean = true): Boolean = {
      (graphChecker.addEdge(edge), edgeTail) match {
        case (NoWays, _) => false
        case (_, Nil) => true
        case (OneWay, x :: xs) => isConnected(x, xs, graphChecker)
        case (TwoWays, x :: xs) =>
          (!edge.isDouble && allowRevert && isConnected(edge.revert, edgeTail, graphChecker.previous, allowRevert = false)) ||
            isConnected(x, xs, graphChecker)
      }
    }

    edges match {
      case Nil => true
      case x :: xs => isConnected(x, xs, new ConnectedGraphChecker)
    }
  }

  private def hasEulerianCycle(edges: List[Edge]): Boolean = {
    def isConnected(edge: Edge, edgeTail: List[Edge], graphChecker: ConnectedGraphChecker, allowRevert: Boolean = true): Boolean = {
      (graphChecker.addEdge(edge), edgeTail) match {
        case (NoWays, _) => false
        case (TwoWays, Nil) => true
        case (_, Nil) => false
        case (OneWay, x :: xs) => isConnected(x, xs, graphChecker)
        case (TwoWays, x :: xs) =>
          (!edge.isDouble && allowRevert && isConnected(edge.revert, edgeTail, graphChecker.previous, allowRevert = false)) ||
            isConnected(x, xs, graphChecker)
      }
    }

    edges match {
      case Nil => true
      case x :: xs => isConnected(x, xs, new ConnectedGraphChecker)
    }
  }
}
