import org.scalatest.FunSuite
import Domino._
import BazarGenerator._
import scala.util.{Random => Rand}

class DominoTest extends FunSuite {

  val RandomCount = 10
  val MinSize = 5
  val MaxSize = 10

  test("isDomino should throw exception when bazar has illegal size") {
    intercept[IllegalArgumentException] {
      assert(isDomino(List(Bone(1, 2))))
    }
  }

  test("isDomino should return true when bazar is domino") {
    assert(isDomino(List(Bone(1, 2), Bone(1, 1), Bone(1, 4), Bone(1, 3), Bone(3, 6))))
    val rand = new Rand
    testRandomBazars(Unit => generate(randomSize(rand), BazarGenerator.Domino), isDomino)
  }

  test("isDomino should return false when bazar isn't domino") {
    assert(!isDomino(List(Bone(1, 3), Bone(3, 4), Bone(5, 6))))
  }

  test("isLine should throw exception when bazar has illegal size") {
    intercept[IllegalArgumentException] {
      assert(isLine(List(Bone(1, 2))))
    }
  }

  test("isLine should return true when bazar is line") {
    assert(isLine(List(Bone(1, 2), Bone(2, 1), Bone(1, 4), Bone(4, 4), Bone(4, 6))))
    val rand = new Rand
    testRandomBazars(Unit => generate(randomSize(rand), Line), isLine)
  }

  test("isLine should return false when bazar isn't line") {
    assert(!isLine(List(Bone(1, 1), Bone(1, 2), Bone(1, 3), Bone(1, 4))))
  }

  test("isRing should throw exception when bazar has illegal size") {
    intercept[IllegalArgumentException] {
      assert(isRing(List(Bone(1, 2))))
    }
  }

  test("isRing should return true when bazar is ring") {
    assert(isRing(List(Bone(1, 2), Bone(2, 1), Bone(1, 4), Bone(4, 4), Bone(4, 1))))
    val rand = new Rand
    testRandomBazars(Unit => generate(randomSize(rand), Ring), isRing)
  }

  test("isRing should return false when bazar isn't ring") {
    assert(!isRing(List(Bone(1, 1), Bone(1, 2), Bone(1, 3), Bone(3, 4))))
  }

  def randomSize(rand: Rand): Int = rand.nextInt(MinSize + 1) + MaxSize - MinSize

  def testRandomBazars(bazarGenerator: Unit => Bazar, checkFunc: Bazar => Boolean): Unit = {
    for (_ <- 1 to RandomCount) {
      val bazar = bazarGenerator()
      assert(checkFunc(bazar), bazar)
    }
  }
}
